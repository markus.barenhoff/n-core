module Network.Flow where

import Control.Lens 
import Data.Hashable      (Hashable, hashWithSalt, hash)
import Data.Hashes.FNV1a  (fnv1aHash32Base, fnv1aHash32Word32, fnv1aHash32Word16, fnvOffsetBasis32)
import Data.List          (intercalate)
import Network.Address

data Flow = Flow { _left     :: Endpoint,
                   _right    :: Endpoint,
                   _protocol :: Protocol
                 }
            deriving Eq

makeFieldsNoPrefix 'Flow

data Direction = Upwards
               | Downwards
               deriving Eq

instance Show Direction where
  showsPrec _ Upwards   = ("upwards" ++)
  showsPrec _ Downwards = ("downwards" ++)

instance Show Flow where
  showsPrec _ flow = (('[':show (flow ^. protocol) ++ ']':endpoints) ++)
    where endpoints = intercalate " => " $ fmap show [ flow ^. left, flow ^. right]

ipWords :: Integral a => IP -> (a, a, a, a)
ipWords (IPv4 addr) = toTuple $ toHostAddress addr
  where toTuple = over each fromIntegral . hostAddressToTuple 
ipWords (IPv6 addr) = over each fromIntegral $ toHostAddress6 addr

instance Hashable Flow where
    -- hash a v6 flow using a Fowler–Noll–Vo hash function, which is supposed to provide
    -- superior uniform distribution at a small performance cost compared to the von Neumann
    -- algorithm as described in RFC 6437
    --
    hashWithSalt salt flow =
        let baseHashValue32          = fromIntegral salt
            (srcD, srcC, srcB, srcA) = flow ^. left . address . to ipWords
            (dstD, dstC, dstB, dstA) = flow ^. left . address . to ipWords 
            addressWords = [srcA, srcB, srcC, srcD, dstA, dstB, dstC, dstD]
            portWords    = [flow ^. left . port , flow ^. right . port]
            h1           = foldl fnv1aHash32Word32 baseHashValue32 addressWords
            h2           = fnv1aHash32Base h1 $ flow ^. protocol . to protocolNumber
            result       = foldl fnv1aHash32Word16 h2 $ fmap fromIntegral portWords
        in case result of
             0 -> 1
             x -> fromIntegral x
    hash = hashWithSalt $ fromIntegral fnvOffsetBasis32

flowFor :: IP -> PortNumber -> IP -> PortNumber -> Protocol -> Flow 
flowFor addrA portA addrB portB _protocol =
    let ((leftAddr, leftPort), (rightAddr, rightPort)) = if addrA < addrB
                                                           then ((addrA, portA), (addrB, portB))
                                                           else ((addrB, portB), (addrA, portA))
    in Flow { _left  = Endpoint leftAddr leftPort  ,
              _right = Endpoint rightAddr rightPort, 
              ..
            }
